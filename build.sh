set -e

rm -rf *pb

for dir in act auth mess gpt db
do
  protoc --go_out=. --go-grpc_out=. ${dir}/${dir}pb.proto
  cd ${dir}pb
    go mod init && go mod tidy
  cd -
done

# git add .
# git commit -m \'${2}\'
# git push
# git tag ${1}
# git push origin --tags